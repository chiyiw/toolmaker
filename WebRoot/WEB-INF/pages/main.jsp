<%@ page import="java.util.*" contentType="text/html; charset=utf-8" %>
<!DOCTYPE HTML>
<html>
<head>
    <title>工具生成器</title>
    <link rel="stylesheet" href="${resourceUrl}css/element-ui.css">
    <link rel="stylesheet" type="text/css" href="${resourceUrl}css/main.css">
</head>
<body>

<div id="app">
    <div class="title">工具生成器</div>
    <el-container style="margin: 10px 10%;">

        <el-main>
            <h1>1. 上传Excel文档</h1>
            <el-upload action="${actionUrl}&action=upload" :file-list="fileList" :on-success="onUpload">
                <el-button slot="trigger" size="small" type="primary">选取文件</el-button>
            </el-upload>

            <h1>2. 编辑展示信息</h1>
            <div>
                <el-form :model="form" ref="form" class="fields-form">
                    <el-form-item
                            v-for="(field, index) in form.fields"
                            :key="field.name"
                            :rules="{required: true, message: '信息不能为空', trigger: 'blur'}">
                        <el-input class="field-name" v-model="form.fields[index].name" placeholder="字段名称"></el-input>
                        <el-select v-model="form.fields[index].type" placeholder="字段类型">
                            <el-option label="文字" value="text"></el-option>
                            <el-option label="数字" value="numeric"></el-option>
                        </el-select>
                        <el-input class="field-length" v-model="form.fields[index].length" placeholder="长度"></el-input>
                        <el-select v-model="form.fields[index].must" placeholder="是否必填">
                            <el-option label="必填" value="true"></el-option>
                            <el-option label="选填" value="false"></el-option>
                        </el-select>
                        <el-button @click.prevent="removeHeader(index)">删除</el-button>
                    </el-form-item>

                    <el-form-item>
                        <el-button type="primary" @click="addHeader">新增信息</el-button>
                        <el-button @click="resetForm">重置</el-button>
                    </el-form-item>
                </el-form>
            </div>
            <h1>3. 填写工具信息</h1>
            <div>
                <el-form :model="toolForm" ref="toolForm" class="fields-form">
                    <el-form-item :rules="{required: true, message: '工具名称不能为空', trigger: 'blur'}">
                        <el-input v-model="toolForm.name" placeholder="工具名称"></el-input>
                    </el-form-item>

                    <el-form-item>
                        <el-button type="primary" @click="makeTool">生成工具</el-button>
                    </el-form-item>
                </el-form>
            </div>
            <h1 v-show="success">4. 下载工具</h1>
            <el-form v-show="success" class="fields-form">
                <p style="font-size: 20px;color: orangered">生成成功!</p>

                <el-form-item>
                    <el-button type="primary" @click="download">下载工具</el-button>
                </el-form-item>
            </el-form>
        </el-main>

    </el-container>

</div>

<script type="text/javascript" src="${resourceUrl}js/jquery-1.8.0.min.js"></script>
<script type="text/javascript" src="${resourceUrl}js/vue.js"></script>
<script type="text/javascript" src="${resourceUrl}js/element-ui.js"></script>
<script type="text/javascript">

    window.g_runToolUrl = '${actionUrl}'; // 数据接口
    window.g_callToolUrl = '${runToolUrl}'; // 工具调用
    window.g_forwardUrl = '${forwardUrl}'; // 跳转接口
    window.g_resourceUrl = '${resourceUrl}'; // 静态资源
    window.g_userId = '${userID}'; // 当前用户账号
    window.g_accessToken = '${accessToken}'; // 当前用户的accessToken
    window.g_bandId = '${bandID}'; // 当前运行的帮区ID
    window.g_rtParam = '${toolParam}'; // 工具运行参数
    window.g_clientType = '${clientType}'; // 工具当前的运行平台
    window.g_thisToolId = '${toolID}'; // 工具的ID
</script>
<script type="text/javascript" src="${resourceUrl}js/main.js"></script>
</body>
</html>