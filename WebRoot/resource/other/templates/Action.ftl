package ${package_name}.action;

import ${package_name}.dao.I${class_name}Dao;
import ${package_name}.dao.impl.${class_name}DaoImpl;
import ${package_name}.entity.${class_name};
import com.fy.toolhelper.tool.ActionTool;
import com.fy.toolhelper.util.FormatUtils;
import com.fy.toolhelper.util.RequestUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.sql.Date;
import java.sql.Time;
import java.text.ParseException;
import java.util.HashMap;
import java.util.Map;

public class ${class_name}Tool extends ActionTool {

    @Override
    protected boolean onInit(GlobalContext context, String configParamStr) throws Exception {
        return false;
    }

    @Override
    protected String onBeforeAct(HttpServletRequest request, HttpServletResponse response,
        TemporyContext context, String runToolParamStr) throws Exception {
        return RequestUtils.getStringParameter(request, "action");
    }

    @Override
    protected Object onCatchException(Throwable ex) throws Exception {
        return null;
    }

    @Override
    protected void onAfterAct(String action, Object result) throws Exception {

    }

    @Action
    public Map<String, Object> getAll${class_name}s(
            HttpServletRequest request,
            HttpServletResponse response) throws Exception {
        I${class_name}Dao dao = new ${class_name}DaoImpl();
        Map<String, Object> result = new HashMap<>();
        result.put("data", dao.getAll${class_name}(getConnection()));
        return result;
    }

    @Action
    public Map<String, Object> delete${class_name}ById(
            HttpServletRequest request,
            HttpServletResponse response) throws Exception {
        Long id = RequestUtils.getLongParameter(request, "id");
        if (id == null) {
            throw new Exception("id参数为空！");
        }
        I${class_name}Dao dao = new ${class_name}DaoImpl();

        Map<String, Object> result = new HashMap<>();
        if (dao.deleteByKeyID(getConnection(), id) > 0) {
            result.put("result", true);
            result.put("msg", "删除成功！");
        } else {
            result.put("result", false);
            result.put("msg", "删除失败！");
        }

        return result;
    }

    @Action
    public Map<String, Object> add${class_name}(
            HttpServletRequest request,
            HttpServletResponse response) throws Exception {

<#if columns??>
    <#list columns as c>
        <#if (c.changedName != 'id') >
            <#if (c.type == 'VARCHAR' || c.type == 'TEXT')>
        String ${c.changedName} = RequestUtils.getStringParameter(request, "${c.changedName}");
            <#elseif (c.type == 'INT' || c.type == 'SMALLINT' || c.type == 'TINYINT')>
        Integer ${c.changedName} = RequestUtils.getIntegerParameter(request, "${c.changedName}");
            <#elseif (c.type == 'BIGINT' || c.type == 'NUMERIC' || c.type == 'DECIMAL')>
        Long ${c.changedName} = RequestUtils.getLongParameter(request, "${c.changedName}");
            <#elseif c.type == 'BIT'>
        boolean ${c.changedName} = RequestUtils.getBooleanParameter(request, "${c.changedName}");
            <#elseif c.type == 'TIMESTAMP' || c.type == 'DATE'>
        Date ${c.changedName} = getDateParameter(request, "${c.changedName}", "yyyy-MM-dd");
            <#elseif c.type == 'TIME'>
        Time ${c.changedName} = getTimeParameter(request, "${c.changedName}");
            <#else>
        String ${c.changedName} = RequestUtils.getStringParameter(request, "${c.changedName}");
            </#if>
        </#if>
    </#list>
</#if>
        I${class_name}Dao dao = new ${class_name}DaoImpl();
        Map<String, Object> result = new HashMap<>();
        ${class_name} ${class_name?uncap_first} = new ${class_name}();

<#if columns??>
    <#list columns as c>
        <#if (c.changedName != 'id')>
        ${class_name?uncap_first}.set${c.changedName?cap_first}(${c.changedName});
        </#if>
    </#list>
</#if>
        try {
            dao.save(getDBConnection(), ${class_name?uncap_first});

            result.put("result", true);
            result.put("msg", "添加成功！");
        } catch (Exception e) {
            result.put("result", false);
            result.put("msg", "添加失败！");
        }
        return result;
    }

    @Action
    public Map<String, Object> edit${class_name}ById(
            HttpServletRequest request,
            HttpServletResponse response) throws Exception {

<#if columns??>
    <#list columns as c>
        <#if (c.changedName == 'id') >
        Long id = RequestUtils.getLongParameter(request, "id");
        <#else>
            <#if (c.type == 'VARCHAR' || c.type == 'TEXT')>
        String ${c.changedName} = RequestUtils.getStringParameter(request, "${c.changedName}");
            <#elseif (c.type == 'INT' || c.type == 'SMALLINT' || c.type == 'TINYINT')>
        Integer ${c.changedName} = RequestUtils.getIntegerParameter(request, "${c.changedName}");
            <#elseif (c.type == 'BIGINT' || c.type == 'NUMERIC' || c.type == 'DECIMAL')>
        Long ${c.changedName} = RequestUtils.getLongParameter(request, "${c.changedName}");
            <#elseif c.type == 'BIT'>
        boolean ${c.changedName} = RequestUtils.getBooleanParameter(request, "${c.changedName}");
            <#elseif c.type == 'TIMESTAMP' || c.type == 'DATE'>
        Date ${c.changedName} = getDateParameter(request, "${c.changedName}", "yyyy-MM-dd");
            <#elseif c.type == 'TIME'>
        Time ${c.changedName} = getTimeParameter(request, "${c.changedName}");
            <#else>
        String ${c.changedName} = RequestUtils.getStringParameter(request, "${c.changedName}");
            </#if>
        </#if>
    </#list>
</#if>
        I${class_name}Dao dao = new ${class_name}DaoImpl();
        Map<String, Object> result = new HashMap<>();
        ${class_name} ${class_name?uncap_first} = new ${class_name}();

<#if columns??>
    <#list columns as c>
        ${class_name?uncap_first}.set${c.changedName?cap_first}(${c.changedName});
    </#list>
</#if>
        try {
            dao.update(getDBConnection(), ${class_name?uncap_first});

            result.put("result", true);
            result.put("msg", "修改成功！");
        } catch (Exception e) {
            result.put("result", false);
            result.put("msg", "修改失败！");
        }
        return result;
    }

    Date getDateParameter(HttpServletRequest request, String name, String format) {
        String temp = request.getParameter(name);
        if(temp != null && !temp.trim().isEmpty()){
            try {
                java.util.Date date = FormatUtils.string2Date(temp, format);
                return new Date(date.getTime());
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    Time getTimeParameter(HttpServletRequest request, String name) {
        String temp = request.getParameter(name);
        if(temp != null && !temp.trim().isEmpty()){
            return Time.valueOf(temp);
        }
        return null;
    }

}
