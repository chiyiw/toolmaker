package ${package_name}.dao.impl;

import ${package_name}.entity.${class_name};
import ${package_name}.dao.I${class_name}Dao;

import com.fy.toolhelper.db.BaseDaoImpl;

import java.sql.Connection;
import java.util.List;

public class ${class_name}DaoImpl extends BaseDaoImpl<${class_name}> implements I${class_name}Dao {

    @Override
    public List<${class_name}> getAll${class_name}(Connection connection) throws Exception {
        return listEntitiesBySql(connection, "SELECT alias.* FROM ${table_name} alias", "alias");
    }

    public ${class_name}DaoImpl() throws Exception {
        super();
    }
}