<%@ page import="java.util.*" contentType="text/html; charset=utf-8" %>
<!DOCTYPE HTML>
<html>
<head>
    <title>${tool_name}</title>
    <link rel="stylesheet" href="${r'${resourceUrl}'}css/element-ui.css">
    <link rel="stylesheet" type="text/css" href="${r'${resourceUrl}'}css/main.css">
</head>
<body>

<div id="app">
    <div class="title">${tool_name}</div>
    <el-container style="margin: 10px 10%;">
        <el-header>所有数据</el-header>

        <el-main>
            <el-table :data="dataList" border max-height="600" align="center" v-loading="loading">
                <#if columns??>
                    <#list columns as c>
                        <#if (c.changedName != 'id')>
                            <#if c.type == 'BIT'>
                <el-table-column prop="${c.changedName}" label="${c.comment!}">
                    <template slot-scope="scope">{{scope.row.${c.changedName} ? '1': '0'}}</template>
                </el-table-column>
                            <#else>
                <el-table-column prop="${c.changedName}" label="${c.comment!}"></el-table-column>
                            </#if>
                        </#if>
                    </#list>
                </#if>
                <el-table-column label="操作" align="right">
                    <template slot-scope="scope">
                        <el-button size="mini" @click="handleEdit(scope.$index, scope.row)">编辑</el-button>
                        <el-button size="mini" type="danger" @click="handleDelete(scope.$index, scope.row)">
                            删除
                        </el-button>
                    </template>
                </el-table-column>
            </el-table>
        </el-main>

        <el-button type="success" icon="el-icon-plus" @click="handleAdd()">添加</el-button>

        <el-dialog :title="formTitle" :visible.sync="formVisible" :before-close="handleClose">
            <el-form :model="form" @submit.native.prevent="commitForm()" style="text-align: left">
            <#if columns??>
                <#list columns as c>
                    <#if (c.changedName != 'id')>
                <el-form-item label="${c.comment}" :label-width="formLabelWidth">
                        <#if c.type == 'BIT'>
                    <el-switch v-model="form.${c.changedName}"></el-switch>
                        <#elseif c.type == 'INT' || c.type == 'BIGINT' || c.type == 'SMALLINT' || c.type == 'TINYINT' || c.type == 'NUMERIC' || c.type == 'DECIMAL'>
                    <el-input v-model="form.${c.changedName}" autocomplete="off" autofocus type="number"></el-input>
                        <#elseif c.type == 'TIMESTAMP' || c.type == 'DATE'>
                    <el-date-picker
                            v-model="form.${c.changedName}" value-format="yyyy-MM-dd" placeholder="选择日期"
                            :picker-options="{
                                shortcuts: [{text: '今天',onClick(picker) {picker.$emit('pick', new Date());}}],
                                disabledDate(time) { return time.getTime() > Date.now(); }
                            }">
                    </el-date-picker>
                        <#elseif c.type == 'TIME'>
                    <el-time-picker
                            v-model="form.${c.changedName}" value-format="HH:mm:ss" placeholder="选择时间">
                    </el-time-picker>
                        <#else>
                    <el-input v-model="form.${c.changedName}" autocomplete="off" autofocus></el-input>
                        </#if>
                </el-form-item>
                    </#if>
                </#list>
            </#if>
            </el-form>
            <div slot="footer" class="dialog-footer">
                <el-button @click="formVisible = false; handleClose(function(){})">取 消</el-button>
                <el-button type="primary" @click="commitForm()">确 定</el-button>
            </div>
        </el-dialog>

    </el-container>

</div>

<script type="text/javascript" src="${r'${resourceUrl}'}js/jquery-1.8.0.min.js"></script>
<script type="text/javascript" src="${r'${resourceUrl}'}js/vue.js"></script>
<script type="text/javascript" src="${r'${resourceUrl}'}js/element-ui.js"></script>
<script type="text/javascript">
    var fyToolUrl = '${r'${fyToolUrl}'}';
    var fyForwardUrl = '${r'${fyForwardUrl}'}';
    var fyResource = '${r'${fyResource}'}';
    var fyCallToolUrl = '${r'${fyCallToolUrl}'}';
    var fyCallToolKey = '${r'${fyCallToolKey}'}';
    var callToolActUrl = '${r'${callToolActUrl}'}';
    var objectID = '${r'${objectID}'}';
    var key;
</script>
<script type="text/javascript" src="${r'${resourceUrl}'}js/main.js"></script>
</body>
</html>