new Vue({
    el: '#app',
    data: {
        loading: true,
        dataList: [],
        form: {
<#if columns??>
    <#list columns as c>
            ${c.changedName}: '',
    </#list>
</#if>
        },
        oldForm: {},
        formVisible: false,
        formTitle: '',
<#--计算表单label宽度，宽度范围[90px ~ 300px]-->
<#assign max_len = 6>
<#if columns??>
    <#list columns as c>
        <#if (c.comment?? && c.comment?length > max_len) >
            <#assign max_len = c.comment?length>
        </#if>
    </#list>
</#if>
        formLabelWidth: '${[max_len, 20]?min * 18}px'
    },
    mounted: function () {
        this.refresh()
    },
    methods: {
        handleAdd: function () {
            this.formTitle = '新增'
            this.formVisible = true
            this.form.action = 'add${class_name}'
        },
        handleEdit: function (index, row) {
            this.formTitle = '修改'
            this.formVisible = true
            this.oldForm = JSON.parse(JSON.stringify(row))
            this.form = row
            this.form.action = 'edit${class_name}ById'
        },
        handleDelete: function (index, row) {
            this.form.id = row.id
            this.form.action = 'delete${class_name}ById'
            this.postForm()
        },
        commitForm: function () {
            this.postForm()
            this.formVisible = false
        },
        refresh: function () {
            var that = this
            $.get(fyToolUrl, {
                action: "getAll${class_name}s"
            }, function (data) {
                that.dataList = data.data
                that.loading = false
            });
        },
        postForm: function () {
            var that = this
            $.post(fyToolUrl, that.form, function (res) {
                if (res.result) {
                    that.refresh()
                    that.resetForm()
                }
                that.$message(res.msg)
            })
        },
        resetForm: function () {
            this.form.action = ''
<#if columns??>
    <#list columns as c>
            this.form.${c.changedName} = ''
    </#list>
</#if>
        },
        handleClose: function (done) {
            var that = this
            for (var key in this.oldForm) {
                if (that.oldForm.hasOwnProperty(key)) {
                    that.form[key] = that.oldForm[key]
                }
            }
            done()
        }
    }
})