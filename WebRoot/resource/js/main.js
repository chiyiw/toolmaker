new Vue({
    el: '#app',
    data: {
        loading: true,
        fileList: [],
        form: {
            fields: [],
        },
        toolForm: {
            name: ''
        },
        toolNamePinyin: '',
        success: false
    },
    mounted: function () {
    },
    methods: {
        onUpload: function (res, file, fileList) {
            console.log(res)
            if (res.code === 200) {
                for (var i = 0; i < res.data.fields.length; i++) {
                    var field = res.data.fields[i]
                    this.form.fields.push({name: field, type: 'text', length: '', must: 'true'})
                }
            }
        },
        addHeader: function() {
            this.form.fields.push({name: '', type: 'text', length: '', must: 'true'});
        },
        removeHeader: function (index) {
            this.form.fields.splice(index, 1)
        },
        makeTool: function() {
            var that = this
            var data = {
                toolName: this.toolForm.name,
                fields: JSON.stringify(that.form.fields)
            }
            this.$refs['form'].validate(function (valid) {
                if (valid) {
                    that.$refs['toolForm'].validate(function (v) {
                        if (v) {
                            that.postForm('makeTool', data)
                        } else {
                            return false
                        }
                    })
                } else {
                    return false
                }
            });
        },
        postForm: function (action, data) {
            var that = this
            $.post(window.g_runToolUrl + '&action=' + action, data, function (res) {
                // that.$message(res.code)
                if (res.code === 200) {
                    that.toolNamePinyin = res.data.toolName
                    that.success = true
                } else if (res.code > 500) {
                    that.success = false
                    alert(res.message)
                } else {
                    that.success = false
                    alert('工具生成失败')
                }
            })
        },
        resetForm: function() {
            this.form.fields = []
        },
        download: function () {
            var options = {
                url: window.g_runToolUrl + '&action=download',
                data: {toolName: this.toolNamePinyin}
            }
            // 创建一个临时的页面下载文件
            var temp_frame = $('<iframe id="download" />');
            var temp_form = $('<form target="download" method="post" />');
            temp_form.attr('action', options.url);
            for (var key in options.data) {
                temp_form.append('<input type="hidden" name="' + key + '" value="' + options.data[key] + '" />');
            }
            temp_frame.append(temp_form);
            $(document.body).append(temp_frame);
            temp_form[0].submit();
            temp_frame.remove();
        }
    }
})