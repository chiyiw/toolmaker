create table t_${应用名称} (
<#list 字段列表 as 字段>
    ${字段.name} <#if 字段.type=="文本">text<#elseif 字段.type=="数字">int</#if><#if 字段.must==true> not null</#if>,
</#list>
) default charset=utf8mb4;

public class C_${应用名称} {
<#list 字段列表 as 字段>
    <#if 字段.type=="文本">String<#elseif 字段.type=="数字">int</#if> ${字段.name};
</#list>
}