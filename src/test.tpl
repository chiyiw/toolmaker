create table t_${应用名称} (
<#loop list=字段列表>
    ${字段名称} <#if 字段类型=文本>text<#elif 字段类型=数字>int</#if><#if 不能为空> not null</#if><#if 有默认值> default ${默认值}</#if><#if 不为最后一个元素>,</#if>
</#loop>
) default charset=utf8mb4;

public class C_${应用名称} {
<#loop list=字段列表>
    <#if 字段类型=文本>String<#elif 字段类型=数字>int</#if> ${字段名称};
</#loop>
}

{
  "应用名称": "学生信息系统",
  "字段列表": [
    {
      "字段名称": "学号",
      "字段类型": "数字",
      "能否为空": "不能为空"
    },
    {
      "字段名称": "姓名",
      "字段类型": "文本"
    },
    {
      "字段名称": "手机号",
      "字段类型": "数字"
    }
  ]
}
