package com.fy.tool.toolmaker.util;

import com.fy.tool.toolmaker.action.Field;

import java.util.List;

public class SQLUtil {

    /**
     * 用户指定的字段长度并不影响数据库字段长度，数据库字段长度一律使用text和numeric即可
     * @param tableName 表名
     * @param fields 字段列表
     * @return create table sql
     */
    public static String toCreateSql(String tableName, List<Field> fields) {
        StringBuilder sqlBuilder = new StringBuilder();
        sqlBuilder.append("create table ").append(tableName).append(" (");
        sqlBuilder.append("id int not null primary key auto_increment,");
        for (int i = 0; i < fields.size(); i++) {
            Field field = fields.get(i);
            sqlBuilder.append("column_").append(i).append(" ").append(field.getType())
                    .append(" comment '").append(fields.get(i).getName()).append("'")
                    .append(",");
        }
        sqlBuilder.deleteCharAt(sqlBuilder.length() - 1);
        sqlBuilder.append(" );");

        return sqlBuilder.toString();
    }
}
