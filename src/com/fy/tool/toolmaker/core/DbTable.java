package com.fy.tool.toolmaker.core;

import lombok.Data;

import java.util.List;

/**
 * 数据库表类
 */
@Data
public class DbTable {

    /**
     * 表名
     */
    String name;

    /**
     * 字段
     */
    List<DbColumn> columns;
}
