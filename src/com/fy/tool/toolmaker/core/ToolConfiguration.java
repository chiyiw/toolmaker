package com.fy.tool.toolmaker.core;

import lombok.Data;

/**
 * 工具配置类
 */
@Data
public class ToolConfiguration {

    /**
     * 工具名称
     */
    String toolName;
    /**
     * 工具包名
     */
    String packageName;
    /**
     * 工具主类名
     */
    String className;

    /**
     * 工具对应的表
     */
    DbTable table;
}
