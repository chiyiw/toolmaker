package com.fy.tool.toolmaker;

import com.fy.tool.toolmaker.action.Field;
import com.fy.tool.toolmaker.core.FreeMarkerUtil;
import freemarker.template.Template;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Test {

    public static void main(String args[]) throws Exception {

        Writer out = new BufferedWriter(new OutputStreamWriter(System.out, StandardCharsets.UTF_8), 10240);

        Map<String, Object> dataMap = new HashMap<>();
        dataMap.put("应用名称", "学生信息系统");

        ArrayList<Field> fields = new ArrayList<Field>(){{
            add(new Field("学号", "数字", 0, true));
            add(new Field("姓名", "文本", 0, false));
            add(new Field("手机号", "数字", 0, false));
        }};
        dataMap.put("字段列表", fields);

        Template template = FreeMarkerUtil.getTemplate("./src", "test.ftl");
        template.process(dataMap, out);
        out.close();
    }
}
