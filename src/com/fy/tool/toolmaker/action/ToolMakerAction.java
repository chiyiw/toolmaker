package com.fy.tool.toolmaker.action;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.fy.basejar.tool.ActionToolBase;
import com.fy.tool.toolmaker.core.DbTable;
import com.fy.tool.toolmaker.core.ToolConfiguration;
import com.fy.tool.toolmaker.core.ToolMakerUtil;
import com.fy.tool.toolmaker.util.PinyinUtil;
import com.fy.tool.toolmaker.util.SQLUtil;
import com.fy.tool.toolmaker.util.ZipUtil;
import com.fy.toolhelper.util.RequestUtils;
import com.fy.tre.utils.RequestUtil;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.sql.Connection;
import java.sql.Statement;
import java.util.*;
import java.util.stream.Collectors;

public class ToolMakerAction extends ActionToolBase {

    private String tempDir = System.getProperty("java.io.tmpdir");

    @Action
    public Map<String, Object> upload(HttpServletRequest request) {

        HashMap<String, Object> result = new HashMap<>();
        HashMap<String, Object> data = new HashMap<>();

        try {
            FileItemFactory factory = new DiskFileItemFactory();
            ServletFileUpload upload = new ServletFileUpload(factory);
            List<FileItem> items = upload.parseRequest(request);

            for (FileItem item : items) {
                if (item.isFormField()) continue;
                // 文件类型的表单
                String fileName = item.getName();
                if (fileName == null) continue;
                InputStream inputStream = item.getInputStream();
                BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));

                ArrayList<List> rows = new ArrayList<>();
                // 读取每一行
                String line;
                boolean readHeader = false;
                while ((line = reader.readLine()) != null) {
                    String[] split = line.split(",");
                    List<String> row = Arrays.stream(split).collect(Collectors.toList());
                    if (!readHeader) {
                        readHeader = true;
                        data.put("fields", row);
                    } else {
                        rows.add(row);
                    }
                }
                data.put("rows", rows);
                result.put("code", 200);
                result.put("data", data);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    @Action
    public Map<String, Object> makeTool(HttpServletRequest request) {
        HashMap<String, Object> result = new HashMap<>();
        HashMap<String, Object> data = new HashMap<>();

        String toolNameChinese = RequestUtils.getStringParameter(request, "toolName");
        JSONArray fieldArray = JSON.parseArray(RequestUtil.getStringParameter(request, "fields"));
        List<JSONObject> fieldJson = fieldArray.stream().map(JSONObject.class::cast).collect(Collectors.toList());
        List<Field> fields = fieldJson.stream()
                .map(f -> new Field(
                        f.getString("name") != null ? f.getString("name") : "",
                        f.getString("type") != null ? f.getString("type") : "text",
                        f.getInteger("length") != null ? f.getInteger("length") : 0,
                        f.getBoolean("must") != null ? f.getBoolean("must") : false))
                .collect(Collectors.toList());

        // 汉字转化为拼音
        String toolName = PinyinUtil.getPingYin(toolNameChinese);
        data.put("toolName", toolName);
        String tableName = "toolmaker_" + toolName;
        // TODO sql 注入问题
        try {
            Connection connection = getDBConnection();
            Map<String, DbTable> dbTables = ToolMakerUtil.getDbTables(connection);
            if (dbTables.get(tableName) != null) {
                result.put("code", 501);
                result.put("message", "相同工具名称已经存在");
                return result;
            }

            String sql = SQLUtil.toCreateSql(tableName, fields);
            Statement statement = connection.createStatement();
            System.out.println(sql);
            statement.executeUpdate(sql);

            dbTables = ToolMakerUtil.getDbTables(connection);
            if (dbTables.get(tableName) != null) {
                System.out.println("数据表生成成功");
            } else {
                result.put("code", 500);
                result.put("message", "生成数据表失败" + dbTables.toString());
                return result;
            }

            // 生成工具
            ToolConfiguration config = new ToolConfiguration();
            config.setToolName(toolNameChinese);
            config.setTable(dbTables.get(tableName));
            config.setClassName("TM_" + toolName);
            config.setPackageName("com.wetoband.tool.toolmaker_" + toolName);

            com.fy.tool.toolmaker.core.ToolMaker maker = new com.fy.tool.toolmaker.core.ToolMaker();
            String outputPath = tempDir + File.separator + "toolmaker_" + toolName;
            maker.setOutputPath(outputPath);
            maker.setToolConfiguration(config);
            maker.setTemplatePath(this.getResourcePath() + File.separator + "other" + File.separator + "templates");
            System.out.println("正在生成工具");
            maker.generate();
            System.out.println("生成工具成功");
            System.out.println("输出项目目录为：" + outputPath);
            data.put("outputDir", outputPath);
            data.put("sql", sql);

            String forwardUrl = this.getForwardUrl();
            String downloadLink = forwardUrl.replace("runTool4ward", "actRunTool") + "&action=download&toolName=" + toolName;
            data.put("downloadLink", downloadLink);

        } catch (Exception e) {
            e.printStackTrace();
            result.put("code", 500);

            result.put("message", this.getResourcePath() + "生成数据表失败" + e.getMessage() + e.getCause() + Arrays.toString(e.getStackTrace()));
            return result;
        }

        result.put("code", 200);
        result.put("data", data);
        return result;
    }

    @Action
    public void download(HttpServletRequest request, HttpServletResponse response) {
        String toolName = RequestUtils.getStringParameter(request, "toolName");
        if (toolName == null || toolName.trim().isEmpty()) {
            System.out.println("工具名称为空");
            return;
        }
        File projectDir = new File(tempDir + File.separator + "toolmaker_" + toolName);
        if (!projectDir.exists()) {
            System.out.println("工具工程目录不存在");
            return;
        }
        try {
            Runtime.getRuntime().exec("python compile.py", null, projectDir).waitFor();

            File file = new File(projectDir.getPath() + File.separator + "tool.zip");
            InputStream ins = null;
            ins = new FileInputStream(file);

            /* 设置文件ContentType类型，这样设置，会自动判断下载文件类型 */
            response.setContentType("multipart/form-data");
            /* 设置文件头：最后一个参数是设置下载文件名 */
            response.setHeader("Content-Disposition", "attachment;filename=" + file.getName());
            OutputStream os = response.getOutputStream();
            byte[] b = new byte[1024];
            int len;
            while ((len = ins.read(b)) > 0) {
                os.write(b, 0, len);
            }
            os.flush();
            os.close();
            ins.close();
        } catch (IOException ioe) {
            ioe.printStackTrace();
        } catch (InterruptedException e) {
            System.out.println("调用外部编译程序失败");
            e.printStackTrace();
        }
    }

    @Action
    public void downloadSource(HttpServletRequest request, HttpServletResponse response) {

        String toolName = RequestUtils.getStringParameter(request, "toolName");
        if (toolName == null || toolName.trim().isEmpty()) {
            System.out.println("工具名称为空");
            return;
        }
        File projectDir = new File(tempDir + File.separator + "toolmaker_" + toolName);
        if (!projectDir.exists()) {
            System.out.println("工具工程目录不存在");
            return;
        }

        String outputFileName = toolName + "-src.zip";
        ZipUtil.zipFolder(projectDir.getAbsolutePath(), projectDir.getAbsolutePath(), outputFileName, "UTF-8");

        File file = new File(projectDir.getAbsolutePath() + File.separator + outputFileName);
        InputStream ins = null;
        try {
            ins = new FileInputStream(file);

            /* 设置文件ContentType类型，这样设置，会自动判断下载文件类型 */
            response.setContentType("multipart/form-data");
            /* 设置文件头：最后一个参数是设置下载文件名 */
            response.setHeader("Content-Disposition", "attachment;filename=" + file.getName());
            OutputStream os = response.getOutputStream();
            byte[] b = new byte[1024];
            int len;
            while ((len = ins.read(b)) > 0) {
                os.write(b, 0, len);
            }
            os.flush();
            os.close();
            ins.close();
            response.flushBuffer();
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }
}
