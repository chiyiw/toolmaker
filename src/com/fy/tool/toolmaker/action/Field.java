package com.fy.tool.toolmaker.action;

public class Field {

    private String name;
    private String type;
    private Integer length;
    private boolean must;

    public Field(String name, String type, Integer length, boolean must) {
        this.name = name;
        this.type = type;
        this.must = must;
        setLength(length);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Integer getLength() {
        return length;
    }

    public void setLength(Integer length) {
        if (length != null) this.length = length;
        this.length = 0;
    }

    public boolean isMust() {
        return must;
    }

    public void setMust(boolean must) {
        this.must = must;
    }
}
